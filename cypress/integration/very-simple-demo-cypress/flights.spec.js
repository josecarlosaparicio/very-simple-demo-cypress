/// <reference types="cypress" />

describe('Visting Flightradar24', () => {
    it.only('Doing a screenshot of the Barajas Airport arrivals', () => {
        cy.visit('https://www.flightradar24.com/40.35,-1.88/6');
        cy.viewport(1920,1080);

        cy.get('.important-banner__close').click();
        cy.get('input[placeholder="Search"]').type('Madrid{enter}');
        cy.get('.ui-menu-item').contains('MAD / LEMD').click();
        cy.get('.airport-info-wrapper.panel-wrapper')
            .find('h2', {timeout: 10000})
            .should('contain.text', 'Madrid Barajas Airport');
        cy.get('[data-testid="arrival-button"]').click();
        cy.get('section[data-component="airportBoard"]')
            .find('[data-testid="onground-flight"]', { timeout: 8000 })
            .should('be.visible');
        cy.screenshot();

    });
});

